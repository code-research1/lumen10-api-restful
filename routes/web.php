<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


use App\Enums\IdentityStatic;

Route::group([
    'middleware' => 'lang.auth'
], function ($router) {

    
    $router->get('/','IndexController@index');
    
    $router->get('/version', function () use ($router) {
        return $router->app->version();
    });


    Route::group([
        'prefix' => IdentityStatic::API_GROUP
    
    ], function ($router) {
    
        
        /*===========THIS LOGIN CONTROLLER============*/
        Route::post('login', 'AuthController@login');
        // Route::post('refresh', 'AuthController@refresh');


        
        /*===========THIS COMMON CONTROLLER============*/
        Route::group([
            'prefix' => 'common'
        
        ], function ($router) {

            Route::post('/encryptData', 'CommonController@encryptData');
            Route::post('/decryptData', 'CommonController@decryptData');
        });
        
    });


    Route::group([
        'middleware' => 'jwt.api'
    ], function ($router) {

        Route::group([
            'prefix' => IdentityStatic::API_GROUP
        
        ], function ($router) {
        
            
            /*===========THIS ROLE CONTROLLER============*/
            Route::group([
                'prefix' => 'role'
            
            ], function ($router) {

                Route::get('/', 'RoleController@index');
                Route::post('/store', 'RoleController@store');
                Route::put('/update/{idMasterRoles}', 'RoleController@update');
                Route::get('/getDataById/{idMasterRoles}', 'RoleController@getDataById');
                Route::put('/updateIsActive/{idMasterRoles}', 'RoleController@updateIsActive');
            });


            
            /*===========THIS USER CONTROLLER============*/
            Route::group([
                'prefix' => 'user'
            
            ], function ($router) {
                Route::get('/', 'UserController@index');
                Route::post('/getAllDataUserByParam', 'UserController@getAllDataUserByParam');
                Route::post('/store', 'UserController@store');
                Route::put('/update/{idMasterUsers}', 'UserController@update');
                Route::get('/getDataById/{idMasterUsers}', 'UserController@getDataById');
                Route::put('/updateIsActive/{idMasterUsers}', 'UserController@updateIsActive');
            
            });
            
        });
        
    });

});
