<?php

namespace App\Repositories\Role\Interfaces;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use LogicException;

interface RoleRepoImpl {

    public function getAll($allParams);

    public function store($data);

    public function update($data, $id);

    public function getDataById($id);

    public function updateIsActive($params, $id);
  
  
}
