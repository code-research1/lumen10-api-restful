<?php

namespace App\Repositories\Role;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use LogicException;
use Illuminate\Support\Str;


use App\Repositories\Role\Interfaces\RoleRepoImpl as RoleRepositoryInterface;
use App\Models\Roles;
use App\Helpers\ResponseService;
use App\Enums\IdentityStatic;

use Auth;
use DB;

class RoleRepository implements RoleRepositoryInterface {


    public function getAll($allParams)
    {

        $query = Roles::query();

        if (Str::length($allParams['search_by']) > 0 && Str::length($allParams['search']) > 0) { 
            $searchBy = urldecode($allParams['search_by']);
            $search = urldecode($allParams['search']);
            $query = $query->where(function($query) use ($search, $searchBy) { 
                $query->where(Str::lower($searchBy), 'ILIKE', '%' . Str::lower($search) . '%');               
            });
        } 

        if (Str::length($allParams['status']) > 0) {
            $query = $query->where('is_active', $allParams['status']);
        }

        $sortDirection = IdentityStatic::SORT_DESC;
        if (Str::length($allParams['sort_direction']) > 0){
            
            if ($allParams['sort_direction'] && in_array($allParams['sort_direction'], ['asc', 'desc'])) {
                $sort = $allParams['sort_direction'];
            }
        }

        if (Str::length($allParams['sort_by']) > 0) { 
            $query = $query->orderBy($allParams['sort_by'], $sort);
        } else {
            $query = $query->orderBy('updated_at', $sort);
        }

        $limit = Str::length($allParams['limit']) > 0 ? $allParams['limit'] : IdentityStatic::PAGINATION_MIN_LIMIT;
        $page  = Str::length($allParams['page']) > 0 ? $allParams['page'] : IdentityStatic::FIRST_PAGE;

        $total = $query->count();

        $result = $query->offset(($page - 1) * $limit)->limit($limit)->get();
        
        return ResponseService::paginateRepository($result, $total);
    }

    public function store($data)
    {
        DB::transaction(function() use($data,&$result) {
           $result = Roles::create($data);
        });
        return $result;
    }

    public function update($data, $id)
    {
        DB::transaction(function() use($data,$id,&$record) {
           $record = Roles::where('id_master_roles',$id)->update($data);
        });
        return $record;
    }

    public function getDataById($id)
    {
        return Roles::where('id_master_roles',$id)->first();
    }

    public function updateIsActive($params, $id)
    {
        DB::transaction(function () use($params,$id,&$update) {
            $update = Roles::where('id_master_roles',$id)->update(["is_active"=>$params['is_active'], "updated_by"=>$params['updated_by']]);
        });
        return $update;
    }


}
