<?php

namespace App\Repositories\User;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use LogicException;
use Illuminate\Support\Str;

use App\Repositories\User\Interfaces\UserRepoImpl as UserRepositoryInterface;
use App\Models\User;
use App\Helpers\ResponseService;
use App\Enums\IdentityStatic;

use Auth;
use DB;

class UserRepository implements UserRepositoryInterface {


    public function getAll($allParams)
    {
    
        $query = User::query();

        if (Str::length($allParams['search_by']) > 0 && Str::length($allParams['search']) > 0) { 
            $searchBy = urldecode($allParams['search_by']);
            $search = urldecode($allParams['search']);
            $query = $query->where(function($query) use ($search, $searchBy) { 
                $query->where(Str::lower($searchBy), 'ILIKE', '%' . Str::lower($search) . '%');               
            });
        } 

        if (Str::length($allParams['status']) > 0) {
            $query = $query->where('is_active', $allParams['status']);
        }

        $sortDirection = IdentityStatic::SORT_DESC;
        if (Str::length($allParams['sort_direction']) > 0){
            
            if ($allParams['sort_direction'] && in_array($allParams['sort_direction'], ['asc', 'desc'])) {
                $sort = $allParams['sort_direction'];
            }
        }

        if (Str::length($allParams['sort_by']) > 0) { 
            $query = $query->orderBy($allParams['sort_by'], $sort);
        } else {
            $query = $query->orderBy('updated_at', $sort);
        }

        $limit = Str::length($allParams['limit']) > 0 ? $allParams['limit'] : IdentityStatic::PAGINATION_MIN_LIMIT;
        $page  = Str::length($allParams['page']) > 0 ? $allParams['page'] : IdentityStatic::FIRST_PAGE;

        $total = $query->count();

        $result = $query->offset(($page - 1) * $limit)->limit($limit)->get();
        
        return ResponseService::paginateRepository($result, $total);
    }

    public function getAllDataUserByParam($data)
    {
        $data = User::where($data['key'] , $data['value'])->first();

        return $data;
    }

    public function store($attributes)
    {
        DB::transaction(function() use($attributes,&$result) {
           $result = User::create($attributes);
        });
        return $result;
    }

    public function update($data, $id)
    {
        DB::transaction(function() use($data,$id,&$record) {
           $record =  User::where('id_master_users',$id)->update($data);
        });
        return $record;
    }

    public function getDataById($id)
    {
        return User::where('id_master_users',$id)->first();
    }

    public function updateIsActive($paramss, $id)
    {
        DB::transaction(function () use($params,$id,&$update) {
            $update = User::where('id_master_users',$id)->update(["is_active"=>$params['is_active'], "updated_by"=>$params['updated_by']]);
        });
        return $update;
    }

}
