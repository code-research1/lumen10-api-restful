<?php

namespace App\Repositories\User\Interfaces;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use LogicException;

interface UserRepoImpl {

    public function getAll($allParams);

    public function getAllDataUserByParam($data);

    public function store($data);

    public function update($data, $id);

    public function getDataById($id);

    public function updateIsActive($params, $id);
}
