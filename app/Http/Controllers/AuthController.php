<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Crypt;

use Mail;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use App\Mail\ForgotPasswordNotification;

use App\Models\User;
use App\Helpers\ResponseService;
use App\Helpers\UtilService;
use App\Enums\ResponseMessage;
use App\Enums\FlaggingStatus;


class AuthController extends Controller
{
    
    public function __construct()
    {
        
    }
    
    public function login(Request $request) : JsonResponse
    {
        $data = UtilService::getJSONRawBody($request);

        if ($data === null) {
            return ResponseService::produceCode(400
                                    , "failed"
                                    , date("Y-m-d H:i:s")
                                    , 'Invalid JSON');
        }
        
        $validator = Validator::make($data, [
            'username'      => 'required|max:255', 
            'password'      => 'required'
        ]);

        if ($validator->fails()) {

            return ResponseService::produceCode(422
                                    , "failed"
                                    , date("Y-m-d H:i:s")
                                    , $validator->errors());
        }

        $check  = User::with(["role"])
                    ->where([["username",$data['username']],["is_active",FlaggingStatus::ACTIVED]])
                    ->first();

        $decryptedPassword = Crypt::decrypt($data['password']);            
        if (Hash::check($decryptedPassword, $check->password) && $check) {
            //return ResponseCode::RESPONSE_SUCCESS($arrData);
            
            $token  = self::createToken($check);
            User::where("id_master_users",$check->id_master_users)->update([
                "updated_at"    => date("Y-m-d H:i:s")
            ]);

            return ResponseService::produceCode(200
                                    , "success"
                                    , date("Y-m-d H:i:s")
                                    , $token);

        }else{
            $errors = [
                "email"   => ["Email / Password not match"]
            ];

            if (app()->getLocale() == "id") {

                $errors = [
                    "email"   => ["Alamat Surel / Kata Sandi tidak cocok"]
                ];

            }

            return ResponseService::ProduceCode(422
                                    , "failed"
                                    , date("Y-m-d H:i:s")
                                    , $errors);
        }
    }


    private function createToken(User $user) : string {

        $payload = [
            // 'sub' => $user,
            'iss' => env('API_KEY_ENCODE'),
            'iat' => time(),
            'exp' => time() + (5 * 24 * 60 * 60), // 5 days in seconds
            "authorized"=> true,
            "jti"=>  Uuid::uuid4()
        ];

        $signatureKey = base64_decode(env('SIGNATURE_KEY_ENCODE'));
        $apiKey = base64_decode(env('API_KEY_ENCODE'));

        $hmac = hash_hmac('sha256', $signatureKey, $apiKey);   
        
        $token = JWT::encode($payload, $hmac, 'HS256');
        
        return $token;
    
    }

  }
