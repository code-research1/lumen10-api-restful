<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Http\JsonResponse;
use Mail;

use App\Helpers\ResponseService;
use App\Enums\ResponseMessage;
use App\Enums\FlaggingStatus;
use App\Enums\IdentityStatic;
use Ramsey\Uuid\Uuid;

class IndexController extends Controller
{
    
    public function __construct()
    {
        
    }
    
    public function index() : JsonResponse
    {
        $message =  env('APP_ENV')." - Welcome Rest Api BWS...";

        return ResponseService::produceCode(200
									, "success"
									, date("Y-m-d H:i:s")
									, $message);
    }
}
