<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Crypt;

use App\Helpers\ResponseService;
use App\Enums\ResponseMessage;
use App\Enums\FlaggingStatus;
use App\Enums\IdentityStatic;
use Ramsey\Uuid\Uuid;

class CommonController extends Controller
{

    public function __construct()
    {
        
    }
    
    public function encryptData(Request $request) : JsonResponse
    {

        $encryptedData = Crypt::encrypt($request->data);
        return ResponseService::produceCode(200
									, "success"
									, date("Y-m-d H:i:s")
									, $encryptedData);

    }

    public function decryptData(Request $request) : JsonResponse
    {

        $decryptedData = Crypt::decrypt($request->encryptedData);
        return ResponseService::produceCode(200
									, "success"
									, date("Y-m-d H:i:s")
									, $decryptedData);

    }


}