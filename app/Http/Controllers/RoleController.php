<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;

use App\Models\Roles;
use App\Helpers\ResponseService;
use App\Enums\ResponseMessage;
use App\Enums\FlaggingStatus;
use App\Enums\IdentityStatic;
use Ramsey\Uuid\Uuid;


use App\Repositories\Role\Interfaces\RoleRepoImpl as RoleRepositoryInterface;

class RoleController extends Controller
{
    private $roleRepo;

    public function __construct(RoleRepositoryInterface $roleRepo)
    {
        $this->roleRepo = $roleRepo;
    }
    
    public function index(Request $request) : JsonResponse
    {

        $allParams = $request->all();
        $response = $this->roleRepo->getAll($allParams);

        $result = ResponseService::paginateController($allParams['page']
                                        , $allParams['limit']
                                        , $response['totalRecordCount']
                                        ,$response['records']);
        
        return ResponseService::produceCode(200
									, "success"
									, date("Y-m-d H:i:s")
									, $result);

    }

    public function store(Request $request) : JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'roleName'      => 'required|max:225|unique:master_roles,role_name',
            'description'   => 'required|max:255',
        ]);  

        if ($validator->fails()) {
            return ResponseService::produceCode(422
									, "error"
									, date("Y-m-d H:i:s")
									, $validator->errors());
        }

        $data = array("id_master_roles" => Uuid::uuid4(),
                      "role_name"       => $request->roleName,
                      "description"     => $request->description,
                      "is_active"       => FlaggingStatus::ACTIVED,
                      "created_by"      => $request->createdBy);
        $result = $this->roleRepo->store($data);
        
        return ResponseService::produceCode(200
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::SUCCESSFULLY_ADD);

    }

    public function update(Request $request, $idMasterRoles) : JsonResponse
    {

        $validator = Validator::make($request->all(), [
            'roleName'      => 'required|max:255|unique:master_roles,role_name,'.$idMasterRoles.',id_master_roles', 
            'description'   => 'required|max:255',
        ]);  

        if ($validator->fails()) {
            return ResponseService::produceCode(422
									, "error"
									, date("Y-m-d H:i:s")
									, $validator->errors());
        }

        $roleCheck = $this->roleRepo->getDataById($idMasterRoles);

        if($roleCheck){
            $data = array("role_name" => $request->roleName,
                        "description"     => $request->description,
                        "updated_by"      => $request->updatedBy);
            $result = $this->roleRepo->update($data, $idMasterRoles);

            return ResponseService::produceCode(200
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::SUCCESSFULLY_UPDATE);
        }else{
            return ResponseService::produceCode(422
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::DATA_NOT_FOUND);
        }
       
    }

    public function getDataById($idMasterRoles)
    {
        $response = $this->roleRepo->getDataById($idMasterRoles);

        if($response){
            return ResponseService::produceCode(200
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , $response);
        } else {
            return ResponseService::produceCode(422
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::DATA_NOT_FOUND);
        }
    }

    public function updateIsActive(Request $request, $idMasterRoles) : JsonResponse
    {

        $roleCheck = $this->roleRepo->getDataById($idMasterRoles);

        if($roleCheck){
            $data = array("is_active" => $request->isActive,
            "updated_by"      => $request->updatedBy);
            $result = $this->roleRepo->updateIsActive($data, $idMasterRoles);

            return ResponseService::produceCode(200
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::SUCCESSFULLY_DELETE);
        }else{
            return ResponseService::produceCode(422
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::DATA_NOT_FOUND);
        }
       
    }

}