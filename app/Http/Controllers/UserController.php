<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;

use App\Models\User;
use App\Helpers\ResponseService;
use App\Helpers\UtilService;
use App\Enums\ResponseMessage;
use App\Enums\FlaggingStatus;
use App\Enums\IdentityStatic;
use Ramsey\Uuid\Uuid;

use App\Repositories\Role\Interfaces\RoleRepoImpl as RoleRepositoryInterface;
use App\Repositories\User\Interfaces\UserRepoImpl as UserRepositoryInterface;

class UserController extends Controller
{

    private $userRepo;
    private $roleRepo;

    public function __construct(UserRepositoryInterface $userRepo, RoleRepositoryInterface $roleRepo)
    {
        $this->userRepo = $userRepo;
        $this->roleRepo = $roleRepo;
    }


    public function index(Request $request) : JsonResponse
    {
        $allParams = $request->all();
        $response = $this->userRepo->getAll($allParams);

        $result = ResponseService::paginateController($allParams['page']
                                        , $allParams['limit']
                                        , $response['totalRecordCount']
                                        ,$response['records']);
        
        
        return ResponseService::produceCode(200
									, "success"
									, date("Y-m-d H:i:s")
									, $result);
    }


    public function getAllDataUserByParam(Request $request) : JsonResponse
    {

        $data = UtilService::getJSONRawBody($request);

        if ($data === null) {
            return ResponseService::produceCode(400
                                    , "failed"
                                    , date("Y-m-d H:i:s")
                                    , 'Invalid JSON');
        }

        $validator = Validator::make($data, [
            'key'       => 'required|max:255', 
            'value'     => 'required|max:255',
        ]);  


        if ($validator->fails()) {
            return ResponseService::produceCode(422
                                    , "failed"
                                    , date("Y-m-d H:i:s")
                                    , $validator->errors());
        }

        $userCheck = $this->userRepo->getAllDataUserByParam($data);
        if($userCheck){
            return ResponseService::produceCode(200
                                    , "success"
                                    , date("Y-m-d H:i:s")
                                    , $userCheck);
        }else{
            return ResponseService::produceCode(422
                                    , "failed"
                                    , date("Y-m-d H:i:s")
                                    , ResponseMessage::DATA_NOT_FOUND);
        }

    }


    public function store(Request $request) : JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'idMasterRoles'     => 'required',
            'fullname'          => 'required|max:255',
            'username'          => 'required|max:225|unique:master_users,username',
            'isGender'          => 'required|max:255',
            'address'           => 'required|max:255',
            'hpNumber'          => 'required|max:255',
            'email'             => 'required|max:255|unique:master_users,email',
        ]);  

        if ($validator->fails()) {
            return ResponseService::produceCode(422
									, "error"
									, date("Y-m-d H:i:s")
									, $validator->errors());
        }

        $roleCheck = $this->roleRepo->getDataById($request->idMasterRoles);

        if($roleCheck){
            $password = UtilService::generatePassword();
            $data = array("id_master_users" => Uuid::uuid4(),
                        "id_master_roles" => $request->idMasterRoles,
                        "fullname"        => $request->fullname,
                        "username"        => $request->username,
                        "is_gender"       => $request->isGender,
                        "address"         => $request->address,
                        "hp_number"       => $request->hpNumber,
                        "email"           => $request->email,
                        "password"        => $password,
                        "created_by"      => $request->createdBy,
                        "is_active"       => FlaggingStatus::ACTIVED);

            $result = $this->userRepo->store($data);
            
            return ResponseService::produceCode(200
                                            , "success"
                                            , date("Y-m-d H:i:s")
                                            , ResponseMessage::SUCCESSFULLY_ADD);
        }else{
            return ResponseService::produceCode(422
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::DATA_NOT_FOUND);
        }
    }

    public function update(Request $request, $idMasterUsers) : JsonResponse
    {

        $validator = Validator::make($request->all(), [
            'idMasterUsers'     => 'required',
            'idMasterRoles'     => 'required',
            'fullname'          => 'required|max:255',
            'username'          => 'required|max:225|unique:master_users,username,'.$idMasterUsers.',id_master_users', 
            'isGender'          => 'required|max:255',
            'address'           => 'required|max:255',
            'hpNumber'          => 'required|max:255',
            'email'             => 'required|max:255|unique:master_users,email,'.$idMasterUsers.',id_master_users',
        ]);  

        if ($validator->fails()) {
            return ResponseService::produceCode(422
									, "error"
									, date("Y-m-d H:i:s")
									, $validator->errors());
        }

        $userCheck = $this->userRepo->getDataById($idMasterUsers);
        $roleCheck = $this->roleRepo->getDataById($request->idMasterRoles);

        if($roleCheck && $userCheck){

            $data = array("id_master_roles" => $request->idMasterRoles,
                        "fullname"        => $request->fullname,
                        "username"        => $request->username,
                        "is_gender"       => $request->isGender,
                        "address"         => $request->address,
                        "hp_number"       => $request->hpNumber,
                        "email"           => $request->email,
                        "updated_by"      => $request->updatedBy);

            $result = $this->userRepo->update($data, $idMasterUsers);

            return ResponseService::produceCode(200
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::SUCCESSFULLY_UPDATE);
        }else{
            return ResponseService::produceCode(422
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::DATA_NOT_FOUND);
        }
       
    }

    public function getDataById($idMasterUsers)
    {
        $response = $this->userRepo->getDataById($idMasterUsers);
        if($response){
            return ResponseService::produceCode(200
                                    , "success"
                                    , date("Y-m-d H:i:s")
                                    , $response);
        } else {
            return ResponseService::produceCode(422
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::DATA_NOT_FOUND);
        }
       
    }

    public function updateIsActive(Request $request, $idMasterUsers) : JsonResponse
    {

        $userCheck = $this->userRepo->getDataById($idMasterUsers);

        if($userCheck){
            $data = array("is_active" => $request->isActive,
                        "updated_by"  => $request->updatedBy);
            $result = $this->roleRepo->updateIsActive($data, $idMasterUsers);

            return ResponseService::produceCode(200
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::SUCCESSFULLY_DELETE);
        }else{
            return ResponseService::produceCode(422
                                        , "success"
                                        , date("Y-m-d H:i:s")
                                        , ResponseMessage::DATA_NOT_FOUND);
        }
       
    }


}