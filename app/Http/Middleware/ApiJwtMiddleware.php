<?php
namespace App\Http\Middleware;
use Closure;
use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\Key;

use App\Helpers\ResponseService;
use App\Enums\ResponseMessage;
use App\Enums\FlaggingStatus;

class ApiJwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
		$token 			= $request->bearerToken();
        // $refreshToken 	= $this->bearerRefreshToken($request);

		$apiKey = $request->header('Api-Key');
		$signature = $request->header('Signature');
		$signatureTime = $request->header('Signature-Time');

		
		if($apiKey != env('API_KEY_ENCODE')) {
			return ResponseService::produceCode(401
									, "error"
									, date("Y-m-d H:i:s")
									, ResponseMessage::HEADER_API_KEY);
		}

		if($signature != env('SIGNATURE_KEY_ENCODE')) {
			return ResponseService::produceCode(401
									, "error"
									, date("Y-m-d H:i:s")
									, ResponseMessage::HEADER_SIGNATURE_KEY);
		}

		if($signatureTime <= 0) {
			return ResponseService::produceCode(401
									, "error"
									, date("Y-m-d H:i:s")
									, ResponseMessage::HEADER_SIGNATURE_TIME);
		}

		
        if(!$token) {
			return ResponseService::produceCode(401
									, "error"
									, date("Y-m-d H:i:s")
									, ResponseMessage::TOKEN_PROVIDED);
        }
		
        try {

			$signatureKey = env('SIGNATURE_KEY');
			$apiKey = env('API_KEY');

			$hmac = hash_hmac('sha256', $signatureKey, $apiKey);   
			
            $credentials = JWT::decode($token, new Key($hmac, 'HS256'));	

        } catch(ExpiredException $e) {
			return ResponseService::produceCode(401
									, "error"
									, date("Y-m-d H:i:s")
									, ResponseMessage::PROVIDED_TOKEN_EXPIRED);
        } catch(Exception $e) {
			return ResponseService::produceCode(401
									, "error"
									, date("Y-m-d H:i:s")
									, ResponseMessage::TOKEN_DECODING);
        }

        // $request->auth 	= $credentials->sub;
        return $next($request);
    }


	public function bearerToken(){
		$header	= $this->header('Authorization','');
		if(Str::startsWith($header, 'Bearer ')){
			return Str::substr($header, 7);
		}
	}


	private function bearerRefreshToken($request){
		$header = explode(':', $request->header('Refresh-Token'));
		$refreshToken = @trim($header[0]);
		return $refreshToken;
	}

	
	// private function getTotalLastLogin($date1){
	// 	$date2 = date("Y-m-d H:i:s");

	// 	$diff = abs(strtotime($date2) - strtotime($date1));

	// 	$years 	= floor($diff / (365*60*60*24));
	// 	$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
	// 	$days 	= floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

	// 	return $days;
	// }

	
	// private function refreshToken($token){
	// 	$user = User::where(["is_active", FlaggingStatus::ACTIVED])->first();
	// 	if($user){
	// 		User::where("id_master_users",$user->id_master_users)->update([
    //             "updated_at"    => date("Y-m-d H:i:s")
    //         ]);

	// 		$payload = [
	// 			'iss' => env('API_KEY_ENCODE'),
	// 			'iat' => time(),
	// 			'exp' => time() + (1440*60*3),
	// 			"authorized"=> true,
	// 			"jti"=>  Uuid::uuid4()
	// 		];
			
	// 		$signatureKey = base64_decode(env('SIGNATURE_KEY_ENCODE'));
	// 		$apiKey = base64_decode(env('API_KEY_ENCODE'));
	
	// 		$hmac = hash_hmac('sha256', $signatureKey, $apiKey);   
			
	// 		return JWT::encode($payload, $hmac, 'HS256');
	// 	}else{
	// 		return false;
	// 	}
	// }

}