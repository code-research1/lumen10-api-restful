<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\User\Interfaces\UserRepoImpl;
use App\Repositories\User\UserRepository;
use App\Repositories\Role\Interfaces\RoleRepoImpl;
use App\Repositories\Role\RoleRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() : void
    {
        $this->app->bind(UserRepoImpl::class, UserRepository::class);
        $this->app->bind(RoleRepoImpl::class, RoleRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() : void
    {
        
    }
}
