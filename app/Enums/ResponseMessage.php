<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ResponseMessage extends Enum
{
    //====== CRUD ======//
    const SUCCESSFULLY_ADD    =   "Successfully Add";
    const SUCCESSFULLY_UPDATE =   "Successfully Update";
    const SUCCESSFULLY_DELETE =   "Successfully Delete";
    const SUCCESSFULLY_SEND   =   "Successfully Send";

    //====== AUTH ======//
    const UNAUTHORIZED_ACCESS    =   "Unauthorized access, you do not have authorized access to this function";
    const DATA_NOT_FOUND         =   "Not found data record";
    const DUPLICATE_DATA         =   "Data duplication";
    const PROVIDED_TOKEN_EXPIRED =   "Provided token is expired";
    const TOKEN_DECODING         =   "An error while decoding token";
    const TOKEN_PROVIDED         =   "Token not provided";


    const HEADER_API_KEY         =   "Header Api-Key not match";
    const HEADER_SIGNATURE_KEY   =   "Header Signature not match";
    const HEADER_SIGNATURE_TIME  =   "Header Signature-Time not match";
    
}
