<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class FlaggingStatus extends Enum
{
    const ACTIVED     = "ACTIVED";
    const DEACTIVED   = "DEACTIVED";

    const TRUE   = true;
    const FALSE  = false;

    const MALE   = "M";
    const FEMALE = "L";
}

