<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class IdentityStatic extends Enum
{
    const ROLE_SUPERADMIN           = "6c1080d1-c0b4-4168-9d40-156eb3385c02";
    const ROLE_DEVELOPER            = "4522e045-80c0-41a0-ad12-da28ab9419b8";

    const EMAIL_NO_REPLY            = "";

    const PAGE_LENGTH               = 10;

    const FIRST_PAGE                = 1;
	const PAGINATION_MIN_LIMIT      = 10;
	const SORT_ASC                  = "asc";
	const SORT_DESC                 = "desc";
	const DEFAULT_SORT_COLOUMN      = "id";

    const API_GROUP         = "/api/v1/lumen10-api-restful";
}
