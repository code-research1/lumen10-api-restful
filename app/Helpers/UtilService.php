<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Illuminate\Support\Facades\Hash;

class UtilService
{

    public static function getJSONRawBody(Request $request)
    {

        // Get the raw request body
        $rawBody = $request->getContent();

        // Parse the raw body as JSON into an associative array
        $data = json_decode($rawBody, true);

        return $data;
    }
    

    public static function generatePassword(){
        $password = "bayuwidiasantoso";
        return Hash::make($password);
    }



}
