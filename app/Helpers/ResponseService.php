<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

use Auth;

class ResponseService
{

    public static function produceCode($responseCode = 200
                                        , $responseDescription = "success"
                                        , $responseTime = null
                                        , $responseDatas = null)
    {   
        $jsonResponse =  new JsonResponse([
            'responseCode' => $responseCode,
            'responseDescription' => $responseDescription,
            'responseTime' => $responseTime,
            'responseDatas' => $responseDatas,
        ], $responseCode);

        $jsonResponse->withHeaders([
            'Content-Type'   => 'application/json',
        ]);

        return $jsonResponse;
    }

    public static function paginateController($pageNumber = 0
                                            , $pageSize = 0
                                            , $totalRecordsCount = 0
                                            , $records = null)
    {   
        $result = Array(
            "pageNumber" => $pageNumber,
            "pageSize" => $pageSize,
            "totalRecordCount" => $totalRecordsCount,
            "records" => $records
        );

        return $result;
    }

    public static function paginateRepository($records = null, $totalRecordsCount = 0)
    {   
        $result = Array(
            "records"=> $records,
            "totalRecordCount"=> $totalRecordsCount
        );

        return $result;
    }
}
