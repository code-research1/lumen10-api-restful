# Lumen Clean Architecture

This project was created to learn php with go laravel/lumen framework

## How To Run

1. Run application with command `php -S localhost:8000 -t public`

## Feature

- [x] Database ORM
- [x] Database Relational
- [x] Json Validation
- [x] JWT Security
- [x] Repository Pattern
- [x] Http Client
- [x] Error Handling
- [x] Encrypt & Decrypt
- [x] Xtra (Exmple Database & Json)